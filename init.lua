local name = "steamwork"
local default_path = minetest.get_modpath(name)

S = minetest.get_translator(name)

dofile(default_path.."/utils.lua")
dofile(default_path.."/boiler.lua")
dofile(default_path.."/pipes.lua")
dofile(default_path.."/tools.lua")
