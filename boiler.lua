minetest.register_node("steamwork:boiler", {
  description = S("Boiler"),
  tiles = {
    "steamwork_boiler.png"
  },
  groups = {
    cracky = 3
  },
  -- on_destruct = function(pos)
  --   minetest.set_node(pos, {name="steamwork:broken_boiler"})
  -- end
})


minetest.register_node("steamwork:broken_boiler", {
  description = S("Broken boiler"),
  tiles = {
    "steamwork_inside_broken_boiler.png",
    "steamwork_boiler.png",
    "steamwork_boiler.png",
    "steamwork_boiler.png",
    "steamwork_boiler.png",
    "steamwork_boiler.png",
    "steamwork_boiler.png",
  },
  groups = {
    cracky = 3,
    not_in_creative_inventory = 1
  },
  drop = "spw:scrap_metal",
  drawtype = "nodebox",
  node_box = {
    type = "fixed",
    fixed = {
      -- bottom
      { px(8), -px(8), px(8), -px(8), -px(7), -px(8)},
      -- +z side
      { px(8), -px(8), px(8), px(7), px(0), px(7)},
      { px(7), -px(8), px(8), px(5), -px(1), px(7)},
      { px(5), -px(8), px(8), px(0), -px(4), px(7)},
      { px(5), -px(8), px(8), -px(2), -px(4), px(7)},
      { -px(2), -px(8), px(8), -px(8), -px(5), px(7)},

      -- -z side
      { -px(8), -px(8), -px(8), -px(6), -px(2), -px(7)},
      { -px(5), -px(8), -px(8), -px(6), -px(3), -px(7)},
      { -px(2), -px(8), -px(8), -px(8), -px(5), -px(7)},
      { px(8), -px(8), -px(8), px(6), -px(5), -px(7)},

      -- +x side
      { px(8), -px(8), px(8), px(7), -px(2), px(5)},
      { px(8), -px(8), -px(8), px(7), -px(5), -px(5)},

      -- -x side
      { -px(8), -px(8), -px(8), -px(7), -px(2), -px(5)},
    }
  },
  selection_box = {
    type = "fixed",
    fixed = {
      { px(8), px(0), px(8), -px(8), -px(8), -px(8)}
    }
  },
  on_construct = function(pos)
    part_id = minetest.add_particlespawner({
      amount = 80,
      time = 8,
      texture = "steamwork_particle_steam.png^[colorize:#fff",
      minpos = {x=pos.x, y=pos.y+0.3, z=pos.z},
      maxpos = {x=pos.x+0.5, y=pos.y+0.3, z=pos.z+0.5},

      minvel = {x=.1, y=.5, z=.1},
      maxvel = {x=.2, y=.8, z=.2},

      minacc = {x=.05, y=1, z=.05},
      maxacc = {x=.2, y=2, z=.2},

      minexptime = 1,
      maxexptime = 5,

      minsize = 2,
      maxsize = 3,
      vertical = false,
      collisiondetection = true
    })
  end
})


local BOILER_MAX_PRESSURE = 5

minetest.register_abm({
  label = "Boiler check steam source",
  nodenames = {
    "steamwork:boiler",
  },
  -- neighbours = { "air" },
  interval = 2,
  chance = 1,
  action = function(pos)
    local meta = minetest.get_meta(pos)

    local b_pos = table.copy(pos)
    b_pos.y = b_pos.y - 1
    local b_node = minetest.get_node(b_pos)

    if b_node.name == "spw:furnace_active" then
      local b_meta = minetest.get_meta(b_pos)
      local b_inv = b_meta:get_inventory()

      -- TODO: check steam source in furnace (bucket with water)

      local pressure = meta:get_float("pressure") or 0

      if pressure < BOILER_MAX_PRESSURE then
        meta:set_float("pressure", pressure + 0.2)
      end
    else
      local pressure = meta:get_float("pressure") or 0

      if pressure > 0 then
        local new_value = pressure - 0.05
        if new_value < 0 then
          new_value = 0
        end

        meta:set_float("pressure", new_value)
      else
        meta:set_float("pressure", 0)
      end
    end
  end
})


minetest.register_abm({
  label = "-",
  nodenames = {
    "steamwork:boiler",
  },
  neighbours = { "steamwork:bronze_pipe" },
  interval = 2,
  chance = 1,
  action = function(pos)
    local meta = minetest.get_meta(pos)

    local t_pos = table.copy(pos)
    t_pos.y = t_pos.y + 1
    local t_node = minetest.get_node(t_pos)

    if is_vertical_pipe(t_node) then
      transfer_pressure(pos, t_pos, 0.2)
    end


    local n_pos = table.copy(pos)
    n_pos.z = n_pos.z + 1
    local n_node = minetest.get_node(n_pos)

    if is_ns_pipe(n_node) then
      transfer_pressure(pos, n_pos, 0.2)
    end


    local s_pos = table.copy(pos)
    s_pos.z = s_pos.z - 1
    local s_node = minetest.get_node(s_pos)

    if is_ns_pipe(s_node) then
      transfer_pressure(pos, s_pos, 0.2)
    end


    local w_pos = table.copy(pos)
    w_pos.x = w_pos.x + 1
    local w_node = minetest.get_node(w_pos)

    if is_we_pipe(w_node) then
      transfer_pressure(pos, w_pos, 0.2)
    end


    local e_pos = table.copy(pos)
    e_pos.x = e_pos.x - 1
    local e_node = minetest.get_node(e_pos)

    if is_we_pipe(e_node) then
      transfer_pressure(pos, e_pos, 0.2)
    end
  end
})


do
  local B = "spw:bronze_ingot"
  local _ = ""
  local P = "steamwork:bronze_pipe"

  minetest.register_craft({
    output = "steamwork:boiler",
    recipe = {
      {B, P, B},
      {P, _, P},
      {B, P, B},
    }
  })
end
