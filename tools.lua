
minetest.register_craftitem("steamwork:pressure_gauge_tool", {
  description = S("Steam pressure gauge tool"),
  wield_scale = {
    x = 1,
    y = 1,
    z = 2
  },
  inventory_image = "steamwork_pressure_gauge_tool.png",
  on_use = function(itemstack, user, pointed_thing)
    if pointed_thing.under then
      local p_node = minetest.get_node(pointed_thing.under)
      local p_meta = minetest.get_meta(pointed_thing.under)
      local pressure = p_meta:get_float("pressure") or 0
      minetest.debug(p_node.name, "PRESSURE", pressure)
    end
  end,
})
