local pixel = 1/16 -- 1x1 coords == 16x16 pixels

function px(count)
  c = count or 1
  return pixel * c
end


function transfer_pressure(from_pos, to_pos, pressure)
  local src_meta = minetest.get_meta(from_pos)
  local src_pressure = src_meta:get_float("pressure") or 0

  local dst_meta = minetest.get_meta(to_pos)
  local dst_pressure = dst_meta:get_float("pressure") or 0

  if dst_pressure > src_pressure then
    src_meta:set_float("pressure", math.min(src_pressure + pressure, 5))
  elseif src_pressure > dst_pressure then
    dst_meta:set_float("pressure", math.min(dst_pressure + pressure, 5))
  end
end


function is_steam_source(pos)
  local node = minetest.get_node(pos)
  return node.name == "steamwork:boiler"
end


function pipe_out_position(pipe_pos)
  local positions = {}

  local node = minetest.get_node(pipe_pos)

  local dirs = {
    [0] = "y",
    [1] = "y",
    [2] = "x",
    [3] = "x",
    [4] = "z",
    [5] = "z",
  }

  local dir = dirs[node.param2]

  local p_pos = table.copy(pipe_pos)
  p_pos[dir] = p_pos[dir] + 1
  table.insert(positions, p_pos)

  local m_pos = table.copy(pipe_pos)
  m_pos[dir] = m_pos[dir] - 1
  table.insert(positions, m_pos)

  return positions
end


function is_vertical_pipe(node)
  return node.name == "steamwork:bronze_pipe" and (node.param2 == 0 or node.param2 == 1)
end


function is_ns_pipe(node)
  -- North to South (NS)
  return node.name == "steamwork:bronze_pipe"
    and (node.param2 == 4 or node.param2 == 5)
end

function is_we_pipe(node)
  -- West to East (WE)
  return node.name == "steamwork:bronze_pipe"
    and (node.param2 == 2 or node.param2 == 3)
end
