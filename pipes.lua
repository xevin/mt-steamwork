
local bronze_pipe_node_box = {
  -- pipe
  {
    px(1), px(8), px(1),
    -px(1), -px(8), -px(1)
  },
  -- flange #1
  {
    px(1.5), px(8), px(1.5),
    -px(1.5), px(7), -px(1.5)
  },
  -- flange #2
  {
    px(1.5), -px(8), px(1.5),
    -px(1.5), -px(7), -px(1.5)
  },
}

minetest.register_node("steamwork:bronze_pipe", {
  description = S("Bronze pipe"),
  tiles = {
    "steamwork_bronze_pipe_flange.png",
    "steamwork_bronze_pipe_flange.png",
    "steamwork_bronze_pipe_body.png",
    "steamwork_bronze_pipe_body.png",
    "steamwork_bronze_pipe_body.png",
    "steamwork_bronze_pipe_body.png",
  },
  groups = { cracky = 3, oddly_breakable_by_hand=1 },
  drawtype = "nodebox",
  sunlight_propagates = true,
  light_source = 3,
  paramtype2 = "wallmounted", -- ставим блок лицом к игроку
  node_box = {
    type = "fixed",
    fixed = bronze_pipe_node_box,
  },
  collision_box = {
    type = "fixed",
    fixed = {
      {
        px(1.5), px(8), px(1.5),
        -px(1.5), -px(8), -px(1.5)
      },
    }
  },
})


local bronze_pipe_l_node_box = {
  -- pipe part 1
  {
    px(1), px(1), px(8),
    -px(1), -px(1), -px(1)
  },
  -- pipe part 2
  {
    px(1), px(1), px(1),
    -px(), -px(8), -px(1)
  },
  -- flange #1
  {
    px(1.5), px(1.5), px(8),
    -px(1.5), -px(1.5), px(7)
  },
  -- flange #2
  {
    px(1.5), -px(8), px(1.5),
    -px(1.5), -px(7), -px(1.5)
  },
}

minetest.register_node("steamwork:bronze_l_pipe", {
  description = S("Bronze L-pipe"),
  tiles = {
    "steamwork_bronze_pipe_body.png",
    "steamwork_bronze_pipe_flange.png",
    "steamwork_bronze_pipe_body.png",
    "steamwork_bronze_pipe_body.png",
    "steamwork_bronze_pipe_flange.png",
    "steamwork_bronze_pipe_body.png",
  },
  groups = { cracky = 3, oddly_breakable_by_hand=1 },
  drawtype = "nodebox",
  sunlight_propagates = true,
  paramtype2 = "wallmounted", -- ставим блок лицом к игроку
  node_box = {
    type = "fixed",
    fixed = bronze_pipe_l_node_box,
  },
  on_rotate = function()
    minetest.debug("L PIPE IS NOT ROTATING BY SCREWDRIVER")
  end
})


local function top_steam_particles(pos)
  part_id = minetest.add_particlespawner({
    amount = 30,
    time = 2,
    texture = "steamwork_particle_steam.png^[colorize:#fff",
    minpos = {x=pos.x, y=pos.y+0.5, z=pos.z},
    maxpos = {x=pos.x, y=pos.y+0.7, z=pos.z},

    minvel = {x=0, y=.5, z=0},
    maxvel = {x=0, y=.8, z=0},

    minacc = {x=.05, y=1, z=.05},
    maxacc = {x=.2, y=2, z=.2},

    minexptime = 1,
    maxexptime = 5,

    minsize = 2,
    maxsize = 3,
    vertical = false,
    collisiondetection = true
  })
end

local function is_vertical_pipe(node)
  return node.param2 == 0 or node.param2 == 1
end

minetest.register_abm({
  label = "",
  nodenames = {
    "steamwork:bronze_pipe",
  },
  neighbours = { "steamwork:boiler" },
  interval = 2,
  chance = 1,
  action = function(pos)
    local node = minetest.get_node(pos)
    local meta = minetest.get_meta(pos)
    local pressure = meta:get_float("pressure") or 0

    if pressure > 0 then
      meta:set_float("pressure", meta:get_float("pressure") - 0.15)

      local out_pos = pipe_out_position(pos)

      local p_pos = out_pos[1]
      local m_pos = out_pos[2]

      if is_steam_source(p_pos) then
        transfer_pressure(pos, m_pos, 0.2)
      elseif is_steam_source(m_pos) then
        transfer_pressure(pos, p_pos, 0.2)
      end

      -- if is_vertical_pipe(node) then
      --   local t_pos = table.copy(pos)
      --   t_pos.y = t_pos.y + 1
      --   local t_node = minetest.get_node(t_pos)
      --   if t_node.name == "air" then
      --     top_steam_particles(pos)
      --   elseif t_node.name == "steamwork:bronze_pipe"
      --     and is_vertical_pipe(t_node)
      --   then
      --     transfer_pressure(pos, t_pos)
      --   end
      -- end
    end
  end
})

do
  local B = "spw:bronze_ingot"
  local _ = ""

  minetest.register_craft({
    output = "steamwork:bronze_pipe 4",
    recipe = {
      {_, B, _},
      {B, _, B},
      {_, B, _},
    }
  })
end
